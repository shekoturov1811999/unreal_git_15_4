#include <iostream>


void function(const int N, int count) // ������ N ���������� � count ��� ������ 
                                     //�������� � �������� ���������� �������

{
    if (count == 1) // ������� ��� ������ ���������
    {
        int count1 = 1;
        while (count1 <= N)
        {
            std::cout << count1 << " " << std::endl;
            count1 += 2;
        }
    }
    else // ������� ��� ������ �������
    {
        int count0 = 0;
        while (count0 <= N)
        {
            std::cout << count0 << " " << std::endl;
            count0 += 2;

        }
    }
}


int main()
{
    
    function(42, 0); /* �����:
                        ������� � ������� ��� ������ ����� �� 0 �� N;
                        �������� �������, ��������� ������� 
                        �������� �������� N � �������� ��������
                     */

    return 0;
}

